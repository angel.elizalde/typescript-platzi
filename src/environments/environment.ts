// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  fireBase: {
    apiKey: 'AIzaSyAULpSjTsv6iVy9mFC3lKZEFUZ0o830EIg',
    authDomain: 'typescript-platzi-95993.firebaseapp.com',
    databaseURL: 'https://typescript-platzi-95993.firebaseio.com',
    projectId: 'typescript-platzi-95993',
    storageBucket: 'typescript-platzi-95993.appspot.com',
    messagingSenderId: '306981545204',
    appId: '1:306981545204:web:35c73e3ffc944b72a1eaa1',
    measurementId: 'G-VE0WP1E83F',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
